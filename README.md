
# Introduction

Traffium is a traffic analysis application, which make the daily driving routine easy and pleasant,
Application continuously follow the user location and current speed and in real time change the route to the more appropriate one.

The application supports Android 8 Oreo (API level 26) and above.

Features include:

  * An easy-to-use interface.

  * **Help to avoid traffic jams**: Help drivers to avoid traffic jams, suggest more suitable routes.

  * **Suitable for truckers**: Application takes into account the main characteristics of the car such a length, width and height.
  
  * **Compatible with Google Maps**: Fully compatible with Google Map application 


# Installation

### From GitHub
```bash
git clone https://gitlab.com/arekko/traffic-analysis-android.git
```

## Building

### With Gradle

This project requires the [Android SDK](http://developer.android.com/sdk/index.html)
to be installed in your development environment. In addition you'll need to set
the `ANDROID_HOME` environment variable to the location of your SDK. For example:

    export ANDROID_HOME=/home/<user>/tools/android-sdk

After satisfying those requirements, the build is pretty simple:

* Run `./gradlew build installDevelopmentDebug` from the within the project folder.
It will build the project for you and install it to the connected Android device or running emulator.

The app is configured to allow you to install a development and production version in parallel on your device.

### With Android Studio
The easiest way to build is to install [Android Studio](https://developer.android.com/sdk/index.html) v2.+
with [Gradle](https://www.gradle.org/) v3.4.1
Once installed, then you can import the project into Android Studio:

1. Open `File`
2. Import Project
3. Select `build.gradle` under the project directory
4. Click `OK`

Then, Gradle will do everything for you.

### Add TomTom API Key
In order to use the project correctly, you need to add a TomTom API key to your Manifest file. Here's how to do that:

1. Go to https://developer.tomtom.com and create a user account for free.
2. TomTom creates a initial key automatically for you. Just go to your dashboard on the developer site and under the tab "Keys" you can find an API key that is enabled for all TomTom API features.
3. If you haven't, clone the android project
4. Open the android manifest file inside the project and replace [YOUR_TOMTOM_API_KEY] with your automatically generated key from hte TomTom site

# Licence
GnuCash Android is free software; you can redistribute it and/or
modify it under the terms of the Apache license, version 2.0.
You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.