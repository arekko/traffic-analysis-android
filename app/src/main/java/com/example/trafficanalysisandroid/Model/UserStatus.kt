package com.example.trafficanalysisandroid.Model

data class UserStatus(val token: String, val navigating: Boolean) {}
