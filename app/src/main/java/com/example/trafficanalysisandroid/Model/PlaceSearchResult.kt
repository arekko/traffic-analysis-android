package com.example.trafficanalysisandroid.Model

data class PlaceSearchResult(
    val fullName: String,
    val lat: Double,
    val lon: Double,
    var isChecked: Boolean
)