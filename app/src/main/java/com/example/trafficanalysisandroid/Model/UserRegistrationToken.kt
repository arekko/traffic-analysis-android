package com.example.trafficanalysisandroid.Model

data class UserRegistrationToken(val registrationToken: String) {}

