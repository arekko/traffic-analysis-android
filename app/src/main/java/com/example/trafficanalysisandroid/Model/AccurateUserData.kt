package com.example.trafficanalysisandroid.Model

import android.os.Parcel
import android.os.Parcelable
import com.example.trafficanalysisandroid.Interfaces.UserData


data class AccurateUserData(
    val userFirebaseToken: String,
    val currentSpeed: Double,
    val sourceCoord: Coordinates,
    override val distCoord: Coordinates,
    override val avoidHighways: Boolean,
    override val isTruck: Boolean,
    override val vehicleWeight: Double?,
    override val vehicleHeight: Double?,
    override val vehicleWidth: Double?
) : UserData, Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readDouble(),
        parcel.readParcelable(Coordinates::class.java.classLoader)!!,
        parcel.readParcelable(Coordinates::class.java.classLoader)!!,
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Double::class.java.classLoader) as? Double
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(userFirebaseToken)
        parcel.writeDouble(currentSpeed)
        parcel.writeParcelable(sourceCoord, flags)
        parcel.writeParcelable(distCoord, flags)
        parcel.writeByte(if (avoidHighways) 1 else 0)
        parcel.writeByte(if (isTruck) 1 else 0)
        parcel.writeValue(vehicleWeight)
        parcel.writeValue(vehicleHeight)
        parcel.writeValue(vehicleWidth)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AccurateUserData> {
        override fun createFromParcel(parcel: Parcel): AccurateUserData {
            return AccurateUserData(parcel)
        }

        override fun newArray(size: Int): Array<AccurateUserData?> {
            return arrayOfNulls(size)
        }
    }
}
