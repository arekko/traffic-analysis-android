package com.example.trafficanalysisandroid.Model

import android.os.Parcel
import android.os.Parcelable
import com.example.trafficanalysisandroid.Interfaces.UserData


data class InitialUserData(
    override val distCoord: Coordinates,
    override val avoidHighways: Boolean = false,
    override val isTruck: Boolean = false,
    override val vehicleWeight: Double? = null,
    override val vehicleHeight: Double? = null,
    override val vehicleWidth: Double? = null
) : UserData, Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readParcelable(Coordinates::class.java.classLoader)!!,
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Double::class.java.classLoader) as? Double
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(distCoord, flags)
        parcel.writeByte(if (avoidHighways) 1 else 0)
        parcel.writeByte(if (isTruck) 1 else 0)
        parcel.writeValue(vehicleWeight)
        parcel.writeValue(vehicleHeight)
        parcel.writeValue(vehicleWidth)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<InitialUserData> {
        override fun createFromParcel(parcel: Parcel): InitialUserData {
            return InitialUserData(parcel)
        }

        override fun newArray(size: Int): Array<InitialUserData?> {
            return arrayOfNulls(size)
        }
    }

}
