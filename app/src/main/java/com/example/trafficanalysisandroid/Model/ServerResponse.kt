package com.example.trafficanalysisandroid.Model

data class ServerResponse(val code: String, val message: String) {}