package com.example.trafficanalysisandroid.Utils

import android.util.Log
import com.example.trafficanalysisandroid.MainActivity
import com.example.trafficanalysisandroid.Model.AccurateUserData
import com.example.trafficanalysisandroid.Model.ServerResponse
import com.example.trafficanalysisandroid.Model.UserRegistrationToken
import com.example.trafficanalysisandroid.Model.UserStatus
import com.example.trafficanalysisandroid.Service.CommunicationService
import com.example.trafficanalysisandroid.Service.MyFirebaseMessagingService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object NetworkManager {

    var baseURL = "http://51.77.192.251:3000"

    /**
     * Send the POST request with current user data to the server
     */
    fun sendUserData(data: AccurateUserData) {

        val retrofit = Retrofit.Builder()
            .baseUrl(baseURL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(CommunicationService::class.java)
        val call = service.postUserData(data)

        call.enqueue(object : Callback<ServerResponse> {
            override fun onResponse(
                call: Call<ServerResponse>,
                response: Response<ServerResponse>
            ) {
                if (response.code() == 200) {
                    val serverResponse = response.body()!!
                    Log.d(MainActivity.TAG, serverResponse.toString())
                }
            }

            override fun onFailure(call: Call<ServerResponse>, t: Throwable) {
                Log.d(MainActivity.TAG, t.message.toString())
            }
        })
    }

    /**
     * Send the POST request with firebase registration token after first run
     */
    fun sendRegistrationToken(userRegistrationToken: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(baseURL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(CommunicationService::class.java)
        val call = service.postUserRegistration(UserRegistrationToken(userRegistrationToken))

        call.enqueue(object : Callback<ServerResponse> {
            override fun onResponse(
                call: Call<ServerResponse>,
                response: Response<ServerResponse>
            ) {
                if (response.code() == 200) {
                    val serverResponse = response.body()!!
                    Log.d(MainActivity.TAG, serverResponse.toString())
                }
            }

            override fun onFailure(call: Call<ServerResponse>, t: Throwable) {
                Log.d(MainActivity.TAG, t.message.toString())
            }
        })
    }

    /**
     * Send the POST request with user status (navigation: boolean)
     */
    fun sendUserStatus(userStatus: UserStatus) {
        val retrofit = Retrofit.Builder()
            .baseUrl(baseURL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(CommunicationService::class.java)
        val call = service.postUserStatus(userStatus)

        call.enqueue(object : Callback<ServerResponse> {
            override fun onResponse(
                call: Call<ServerResponse>,
                response: Response<ServerResponse>
            ) {
                if (response.code() == 200) {
                    val serverResponse = response.body()!!
                    Log.d(MainActivity.TAG, serverResponse.toString())
                }
            }

            override fun onFailure(call: Call<ServerResponse>, t: Throwable) {
                Log.d(MainActivity.TAG, t.message.toString())
            }
        })
    }
}