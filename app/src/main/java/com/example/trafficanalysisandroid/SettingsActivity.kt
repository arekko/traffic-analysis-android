package com.example.trafficanalysisandroid

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.settings, SettingsFragment())
            .commit()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    class SettingsFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)

            val sharedPref = preferenceManager.sharedPreferences

            val vehicleWeightPref: Preference? = findPreference("weight")
            val vehicleHeightPref: Preference? = findPreference("height")
            val vehicleWidthtPref: Preference? = findPreference("width")

            vehicleWeightPref?.summary = sharedPref.getString("weight", "")
            vehicleHeightPref?.summary = sharedPref.getString("height", "")
            vehicleWeightPref?.summary = sharedPref.getString("width", "")

            vehicleWeightPref?.setOnPreferenceChangeListener { _, value ->
                vehicleWeightPref.summary = value as String
                true
            }

            vehicleHeightPref?.setOnPreferenceChangeListener { _, value ->
                vehicleHeightPref.summary = value as String
                true
            }

            vehicleWidthtPref?.setOnPreferenceChangeListener { _, value ->
                vehicleWidthtPref.summary = value as String
                true
            }
        }
    }
}