package com.example.trafficanalysisandroid

import android.widget.SearchView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.coroutineScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SearchQueryTextListener(
    lifecycle: Lifecycle,
    private val SearchQueryCallback: (String?) -> Unit
) : SearchView.OnQueryTextListener {

    private val debouncePeriod: Long = 500

    private val coroutineScope: CoroutineScope = lifecycle.coroutineScope
    private var searchJob: Job? = null

    override fun onQueryTextChange(s: String?): Boolean {

        searchJob?.cancel()
        searchJob = coroutineScope.launch {
            s?.let {
                delay(debouncePeriod)
                SearchQueryCallback(s)
            }
        }

        return false
    }

    override fun onQueryTextSubmit(s: String?): Boolean {
        return false
    }

}