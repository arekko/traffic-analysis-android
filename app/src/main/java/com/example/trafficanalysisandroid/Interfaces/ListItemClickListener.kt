package com.example.trafficanalysisandroid.Interfaces

import com.tomtom.online.sdk.search.data.fuzzy.FuzzySearchResult

interface ListItemClickListener {

    fun autoCompleteItemClicked(searchResult: FuzzySearchResult)

    fun searchHistoryItemClicked(position: Int)

}