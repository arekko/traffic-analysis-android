package com.example.trafficanalysisandroid.Interfaces

import com.example.trafficanalysisandroid.Model.Coordinates

interface UserData  {
    val distCoord: Coordinates
    val avoidHighways: Boolean
    val isTruck: Boolean
    val vehicleWeight: Double?
    val vehicleHeight: Double?
    val vehicleWidth: Double?
}

