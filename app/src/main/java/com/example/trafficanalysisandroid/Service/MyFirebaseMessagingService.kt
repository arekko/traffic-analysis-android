package com.example.trafficanalysisandroid.Service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.trafficanalysisandroid.MainActivity
import com.example.trafficanalysisandroid.Model.AccurateUserData
import com.example.trafficanalysisandroid.Model.Coordinates
import com.example.trafficanalysisandroid.Model.InitialUserData
import com.example.trafficanalysisandroid.Model.UserStatus
import com.example.trafficanalysisandroid.R
import com.example.trafficanalysisandroid.Utils.NetworkManager
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


const val TAG = "FBASE"

class MyFirebaseMessagingService : FirebaseMessagingService(), LocationListener {


    private var lastLocation: Location? = null
    private var calculatedSpeed: Double = 0.0
    private var currentUserData: AccurateUserData? = null
    private var initialUserData: InitialUserData? = null

    /**
     * Store user Firebase registration token
     * Store user Firebase registration token
     */
    private var userFirebaseToken: String? = null


    lateinit var locationManager: LocationManager

    /**
     * How often will the GPS produce data, at minimum.
     */
    private val minimumUpdateInterval = 3000L //ms

    /**
     * Store the navigation state, if true means user starts to navigate
     */
    private var navigating = false

    /**
     * How much the GPS has to move to produce new data, at minimum.
     * Higher value helps reduce the noise.
     */
    private val minimumUpdateDistance = 1f //m

    /**
     * Listen the receiving data from main activity
     */
    private val mMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {

            Log.d(TAG, "get from main activity" + intent!!.extras!!.get("navigation"))
            Log.d(TAG, "get userData main activity" + intent.getParcelableExtra("userData"))

            navigating = intent.extras!!.getBoolean("navigation")
            initialUserData = intent.getParcelableExtra("userData")
        }

    }

    override fun onLocationChanged(location: Location?) {
        Log.d(DBG, "lon" + location?.longitude?.toString())
        Log.d(DBG, "lat" + location?.latitude?.toString())
        Log.d(DBG, "time" + location?.time?.toString())

        if (lastLocation != null) {
            val elapsedTime: Double =
                ((location!!.time.toDouble()) - (lastLocation!!.time.toDouble())) / 1000
            calculatedSpeed = (lastLocation!!.distanceTo(location) / elapsedTime) * 3.6
            Log.d(TAG, "speed $calculatedSpeed")

            if (initialUserData != null) {
                currentUserData = AccurateUserData(
                    userFirebaseToken ?: "",
                    calculatedSpeed,
                    Coordinates(location.longitude, location.latitude),
                    initialUserData!!.distCoord,
                    initialUserData!!.avoidHighways,
                    initialUserData!!.isTruck,
                    initialUserData!!.vehicleWeight,
                    initialUserData!!.vehicleHeight,
                    initialUserData!!.vehicleWidth
                )

            }
        }

        this.lastLocation = location


    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

        Log.d(DBG, provider.toString())
    }

    override fun onProviderEnabled(provider: String?) {
        Log.d(DBG, provider.toString())
    }

    override fun onProviderDisabled(provider: String?) {
        Log.d(DBG, provider.toString())
    }

    override fun onCreate() {
        super.onCreate()
        retrieveCurrentRegistrationToken()

        LocalBroadcastManager.getInstance(this).registerReceiver(
            (mMessageReceiver),
            IntentFilter("MyData")
        )

        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        try {
            locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                minimumUpdateInterval,
                minimumUpdateDistance,
                this
            )
            locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                minimumUpdateInterval,
                minimumUpdateDistance,
                this
            )
        } catch (e: SecurityException) {
            Log.d(DBG, e.toString())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver)

    }


    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)

        Log.d(TAG, "Receive the message" + remoteMessage?.notification.toString())

        /**
         * Check if message contains a data payload, do some action
         */
        remoteMessage?.data?.isNotEmpty()?.let {
            Log.d(TAG, "Message data payload: " + remoteMessage.data)

            // Compose and show notification
            if (!remoteMessage.data.isNullOrEmpty()) {
                val type: String = remoteMessage.data.get("type").toString()
                Log.d(TAG, type)

                if (type == "status") {

                    NetworkManager.sendUserStatus(
                        UserStatus(
                            userFirebaseToken ?: "",
                            navigating
                        )
                    )

                } else if (type == "location") {
                    if (currentUserData != null) {
                        NetworkManager.sendUserData(currentUserData!!)
                    }

                } else if (type == "url") {

                    val msg: String = remoteMessage.data.get("message").toString()

                    sendNotification(msg)
                }
            }
        }

        /**
         * Check if message contains a notification payload, push notification payload
         */
        remoteMessage?.notification?.let {
//            sendNotification(remoteMessage.notification?.body, remoteMessage.notification?.title)
        }
    }


    /**
     * Update user firebase token
     */
    override fun onNewToken(token: String?) {
        super.onNewToken(token)
        this.userFirebaseToken = token
        if (token != null) {
            NetworkManager.sendRegistrationToken(token)
        }
        Log.d(TAG, "Refreshed token: $token")
    }

    /**
     * Show notification to the user
     */
    private fun sendNotification(messageBody: String?) {

        val gmIntentUri = Uri.parse(messageBody)
        val gmIntent = Intent(Intent.ACTION_VIEW, gmIntentUri)
        gmIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        gmIntent.setClassName(
            "com.google.android.apps.maps",
            "com.google.android.maps.MapsActivity"
        )

        val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, gmIntent, 0)

//        val intent = Intent(this, MainActivity::class.java)
//
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//
//        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.ic_stat_ic_notification)
            .setContentTitle("Your new route")
            .setContentText("Click to open new route")
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        // https://developer.android.com/training/notify-user/build-notification#Priority
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(0, notificationBuilder.build())
    }

    /**
     * Retrieve the user Firebase registration token
     */
    private fun retrieveCurrentRegistrationToken() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(MainActivity.TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token
                if (token != null) {
                    this.userFirebaseToken = token
                }
            })
    }

    companion object {
        const val BASE_URL = "http://51.77.192.251:3000"
        private var PRIVATE_MODE = 0
        private val APP_STATUS = "APP_STATUS"
    }
}