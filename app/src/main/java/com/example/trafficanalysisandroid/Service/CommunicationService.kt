package com.example.trafficanalysisandroid.Service

import com.example.trafficanalysisandroid.Model.*
import retrofit2.http.Body
import retrofit2.http.POST

/** Provide the retrofit request methods */
interface CommunicationService {
    @POST("/navigate")
    fun postUserData(@Body userData: AccurateUserData): retrofit2.Call<ServerResponse>

    @POST("/register")
    fun postUserRegistration(@Body userRegistrationToken: UserRegistrationToken): retrofit2.Call<ServerResponse>

    @POST("/status")
    fun postUserStatus(@Body userStatus: UserStatus): retrofit2.Call<ServerResponse>
}


