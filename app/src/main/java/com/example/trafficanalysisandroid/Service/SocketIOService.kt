package com.example.trafficanalysisandroid.Service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.trafficanalysisandroid.MainActivity
import com.example.trafficanalysisandroid.R
import com.github.nkzawa.emitter.Emitter
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import java.net.URISyntaxException

const val SOCKET_URL = "http://192.168.1.205:3000"
const val EDUROAM = "http://10.69.49.52:3000"
const val DBG = "DBG"

class SocketIOService : Service(), LocationListener {

    private lateinit var mSocket: Socket
    private var mRunning = false

    // Unique Identification Number for the Notification.
    // We use it on Notification start, and to cancel it.
    private val NOTIFICATION = "UNIQUE"

    lateinit var locationManager: LocationManager

    /**
     * How often will the GPS produce data, at minimum.
     */
    private val minimumUpdateInterval = 3000L //ms

    /**
     * How much the GPS has to move to produce new data, at minimum.
     * Higher value helps reduce the noise.
     */
    private val minimumUpdateDistance = 3f //m


    override fun onLocationChanged(location: Location?) {
        Log.d(DBG, location?.speedAccuracyMetersPerSecond.toString())
        Log.d(DBG, location?.speed?.toDouble().toString())

        Log.d(DBG, mSocket.connected().toString())
        mSocket.emit("location", location?.latitude.toString());

    }


    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

        Log.d(DBG, provider.toString())
    }

    override fun onProviderEnabled(provider: String?) {
        Log.d(DBG, provider.toString())
    }

    override fun onProviderDisabled(provider: String?) {
        Log.d(DBG, provider.toString())
    }


    override fun onCreate() {
        /**
         * Initialize the location  sensors.
         */

        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        try {
            locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                minimumUpdateInterval,
                minimumUpdateDistance,
                this
            )
            locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                minimumUpdateInterval,
                minimumUpdateDistance,
                this
            )
        } catch (e: SecurityException) {
            Log.d(DBG, e.toString())
        }

    }


    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        if (!mRunning) {
            mRunning = true;

            try {
                mSocket = IO.socket(EDUROAM)

                //create connection
                mSocket.connect()
                Log.d(DBG, mSocket.connected().toString())

                mSocket.emit("transmit");

                mSocket.on("gmUrl", onGMUrl);

            } catch (e: URISyntaxException) {
                e.printStackTrace()
            }



            createNotificationChannel()
            val notificationIntent = Intent(this, MainActivity::class.java)
            val pendingIntent = PendingIntent.getActivity(
                this,
                0, notificationIntent, 0
            )

            val notification = NotificationCompat.Builder(this, NOTIFICATION)
                .setContentTitle(getText(R.string.local_service_label))
                .setContentText("Traffium is running")
                .setSmallIcon(R.drawable.ic_directions_notif)
                .setColor(getColor(R.color.colorBlue))
                .setContentIntent(pendingIntent)
                .build()

            startForeground(1, notification)


        }

        return super.onStartCommand(intent, flags, startId)
    }

    private val onGMUrl = Emitter.Listener { args ->
        Log.d(DBG, "receive the message::")
        Log.d(DBG, args[0].toString())
        val gmURI: String = args[0] as String
        createGMNotificationChannel()
        pushNotification(gmURI)
    }


    private fun createNotificationChannel() {
        // Create the notification channel required by android to keep the service alive
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                NOTIFICATION,
                "Foreground Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )

            val manager = getSystemService(NotificationManager::class.java)
            manager!!.createNotificationChannel(serviceChannel)
        }
    }


    /**
     * Google map notification setup
     */

    private fun createGMNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Your new route"
            val descriptionText = "Please click to start new route"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(GM_CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }



    private fun pushNotification(uri: String) {
        val gmIntentUri = Uri.parse(uri)

        val gmIntent = Intent(Intent.ACTION_VIEW, gmIntentUri)
        gmIntent.setClassName(
            "com.google.android.apps.maps",
            "com.google.android.maps.MapsActivity"
        )

        val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, gmIntent, 0)

        val builder = NotificationCompat.Builder(this,
            GM_CHANNEL_ID
        )
            .setSmallIcon(R.drawable.ic_location)
            .setContentTitle("Your route")
            .setContentText("Please click to start new route")
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)

        val notificationID = 1337
        with(NotificationManagerCompat.from(this)) {
            notify(notificationID, builder.build())
        }
    }


    companion object {
        const val GM_CHANNEL_ID = "TrafficAnalysisDemo"
    }


}
