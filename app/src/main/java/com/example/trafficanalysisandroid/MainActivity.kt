package com.example.trafficanalysisandroid

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.edit
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.trafficanalysisandroid.Adapters.AutoCompleteRVAdapter
import com.example.trafficanalysisandroid.Adapters.SearchListRVAdapter
import com.example.trafficanalysisandroid.Interfaces.ListItemClickListener
import com.example.trafficanalysisandroid.Model.Coordinates
import com.example.trafficanalysisandroid.Model.InitialUserData
import com.example.trafficanalysisandroid.Model.PlaceSearchResult
import com.google.gson.Gson
import com.tomtom.online.sdk.search.OnlineSearchApi
import com.tomtom.online.sdk.search.data.fuzzy.FuzzySearchQueryBuilder
import com.tomtom.online.sdk.search.data.fuzzy.FuzzySearchResponse
import com.tomtom.online.sdk.search.data.fuzzy.FuzzySearchResult
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity


class MainActivity : AppCompatActivity(), ListItemClickListener {

    private lateinit var autoCompleteRV: RecyclerView
    private lateinit var autoCompleteVA: RecyclerView.Adapter<*>
    private lateinit var autoCompleteLM: RecyclerView.LayoutManager

    private lateinit var searchListRV: RecyclerView
    private lateinit var searchListVA: RecyclerView.Adapter<*>
    private lateinit var searchListLM: RecyclerView.LayoutManager

    private lateinit var searchResultSet: Set<String>
    private var currentSelectedIndex: Int = 0

    private var autoCompleteResults: ArrayList<FuzzySearchResult> = ArrayList()
    private var searchListResults: ArrayList<PlaceSearchResult> = ArrayList()

    private var broadcaster: LocalBroadcastManager? = null

    var distCoordinates: Coordinates? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Check ACCESS_FINE_LOCATION permissions
        checkPermissions()
        title = "Traffium"

        // Create the LocalBroadcastManager
        broadcaster = LocalBroadcastManager.getInstance(this)

        // Create new intent
        intent = Intent(MY_INTENT)

        setupAutocomplete()
        loadSearchHistory()


        autoCompleteLM = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        searchListLM = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        autoCompleteVA = AutoCompleteRVAdapter(
            autoCompleteResults,
            this
        )
        searchListVA = SearchListRVAdapter(
            searchListResults,
            this
        )

        autoCompleteRV = findViewById<RecyclerView>(R.id.auto_complete_list_rv).apply {
            setHasFixedSize(true)
            layoutManager = autoCompleteLM
            adapter = autoCompleteVA
        }
        searchListRV = findViewById<RecyclerView>(R.id.past_search_list_rv).apply {
            setHasFixedSize(true)
            layoutManager = searchListLM
            adapter = searchListVA
        }

        btn_start.setOnClickListener {
            if (distCoordinates == null) return@setOnClickListener

            clickNavigation()
        }

        auto_complete_list_rv.visibility = View.GONE
        destinationInputField.setOnQueryTextFocusChangeListener { _, b ->
            if (b) {
                auto_complete_list_rv.visibility = View.VISIBLE
                search_parameter_container.visibility = View.GONE
                autoCompleteResults.clear()
                autoCompleteVA.notifyDataSetChanged()
            } else {
                auto_complete_list_rv.visibility = View.GONE
                search_parameter_container.visibility = View.VISIBLE
                autoCompleteResults.clear()
                autoCompleteVA.notifyDataSetChanged()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu1 -> startActivity<SettingsActivity>()
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * Loads the previously searched destinations from shared preferences
     */
    private fun loadSearchHistory() {
        val preferences = getPreferences(Context.MODE_PRIVATE)
        val gson = Gson()
        var check = false
        if (preferences.contains(SEARCH_LIST_KEY)) {
            searchResultSet =
                HashSet<String>(preferences.getStringSet(SEARCH_LIST_KEY, HashSet<String>())!!)
            searchResultSet.forEachIndexed { index, element ->
                val data = gson.fromJson(element, PlaceSearchResult::class.java)
                if (data.isChecked) {
                    if (!check) {
                        currentSelectedIndex = index
                        check = true
                    } else {
                        data.isChecked = false
                    }
                }
                searchListResults.add(data)
            }
        } else {
            val set = HashSet<String>()
            preferences.edit {
                putStringSet(SEARCH_LIST_KEY, set)
                commit()
            }
        }
    }

    /**
     * Setup autocomplete feature
     */
    private fun setupAutocomplete() {
        destinationInputField.setOnQueryTextListener(
            SearchQueryTextListener(this@MainActivity.lifecycle) { searchString ->
                run {
                    searchString?.let {
                        if (!it.isBlank() && it.isNotEmpty() && it.length > 2) {

                            val searchAPI = OnlineSearchApi.create(this)!!
                            val searchQuery = FuzzySearchQueryBuilder.create(it)
                                .withTypeAhead(true)
                                .build()

                            searchAPI.search(searchQuery).subscribe(
                                { response -> lookAtSearchResults(response) },
                                { error -> Log.d("SEARCH", "Error: ${error.message}") }
                            )
                        }
                    }
                }
            })
    }

    /**
     * Filters the newly found search results and updates the
     * recycler view
     */
    private fun lookAtSearchResults(response: FuzzySearchResponse) {
        val list = response.results
        autoCompleteResults.clear()
        if (list.size > 0) {
            for (i in 0..10) {
                if (list.size > i) {
                    autoCompleteResults.add(list[i])
                }
            }
        }
        autoCompleteVA.notifyDataSetChanged()
    }

    /**
     * Callback for btn_start. Saves all set parameters and creates google maps
     * link for new intent
     */
    private fun clickNavigation() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val isTruck = prefs.getBoolean("truck", false)
        val isHighway = prefs.getBoolean("highway", false)
        val vehicleWidth = prefs.getString("width", "").toString()
        val vehicleWeight = prefs.getString("weight", "").toString()
        val vehicleHeight = prefs.getString("height", "").toString()

        val userData: InitialUserData
        if (isTruck &&
            vehicleHeight.isNotEmpty() &&
            vehicleWidth.isNotEmpty() &&
            vehicleWeight.isNotEmpty()
        ) {
            userData = InitialUserData(
                distCoordinates!!,
                isHighway,
                isTruck,
                vehicleHeight.toDouble(),
                vehicleWeight.toDouble(),
                vehicleWidth.toDouble()
            )
        } else {
            userData = InitialUserData(
                distCoordinates!!,
                isHighway,
                isTruck
            )
        }

        intent.putExtra("navigation", true)
        intent.putExtra("userData", userData)


        broadcaster!!.sendBroadcast(intent)


        val gmURI =
            "https://www.google.com/maps/dir/?api=1" +
                    "&destination=" + distCoordinates!!.lat + "," + distCoordinates!!.lon +
                    "&travelmode=driving" +
                    "&dir_action=navigate"

        startNavigation(gmURI)
    }

    /**
     * Dispatches to the google map intent for navigation
     */
    private fun startNavigation(uri: String) {
        val gmIntentUri = Uri.parse(uri)

        val gmIntent = Intent(Intent.ACTION_VIEW, gmIntentUri)
        gmIntent.setClassName(
            "com.google.android.apps.maps",
            "com.google.android.maps.MapsActivity"
        )
        startActivity(gmIntent)
    }

    /**
     * Selects picked search history item and updates the view
     */
    private fun checkSelectedItem(position: Int) {
        if (searchListResults.size > currentSelectedIndex) {
            searchListResults[currentSelectedIndex].isChecked = false
        }

        val item = searchListResults[position]
        item.isChecked = true
        distCoordinates = Coordinates(item.lon, item.lat)

        searchListVA.notifyDataSetChanged()
        currentSelectedIndex = position
    }

    /**
     * Updates shared preferences for search history
     */
    private fun updateInternalSearchHistory() {
        val preferences = getPreferences(Context.MODE_PRIVATE)
        val gson = Gson()
        val set = HashSet<String>()
        searchListResults.forEach {
            set.add(gson.toJson(it))
        }
        preferences.edit {
            putStringSet(SEARCH_LIST_KEY, set)
            commit()
        }
    }

    /**
     * Checks if searched location already exists in history and adds it if not
     */
    private fun isSearchResultAlreadyExisting(psr: PlaceSearchResult): Boolean {
        searchListResults.forEach {
            if (it.fullName.equals(psr.fullName, true)) {
                return true
            }
        }
        searchListResults.add(0, psr)
        searchListVA.notifyDataSetChanged()
        ++currentSelectedIndex
        return false
    }

    /**
     * Auto complete item clicked callback
     */
    override fun autoCompleteItemClicked(searchResult: FuzzySearchResult) {
        val placeString = searchResult.address.freeformAddress
        distCoordinates = Coordinates(searchResult.position.longitude, searchResult.position.latitude)

        destinationInputField.clearFocus()
        destinationInputField.setQuery(placeString, false)

        autoCompleteResults.clear()
        autoCompleteVA.notifyDataSetChanged()

        val placeSearchResult =
            PlaceSearchResult(placeString, searchResult.position.latitude, searchResult.position.longitude, true)

        if (!isSearchResultAlreadyExisting(placeSearchResult)) {
            updateInternalSearchHistory()
        }

        checkSelectedItem(0)
    }

    /**
     * History item clicked callback
     */
    override fun searchHistoryItemClicked(position: Int) {
        checkSelectedItem(position)
    }

    private fun checkPermissions() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
        ) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    123
                )
            }
        } else {
            // Permission has already been granted
        }
    }

    companion object {
        const val TAG = "TAG"
        const val SEARCH_LIST_KEY = "SEARCH_LIST_KEY"
        private var PRIVATE_MODE = 0
        private val APP_STATUS = "APP_STATUS"
        private val USER_DATA = "USER_DATA"
        val MY_INTENT = "MyData"
    }

}
