package com.example.trafficanalysisandroid.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.trafficanalysisandroid.Interfaces.ListItemClickListener
import com.example.trafficanalysisandroid.Model.PlaceSearchResult
import com.example.trafficanalysisandroid.R
import com.google.android.material.card.MaterialCardView

class SearchListRVAdapter(
    private val data: ArrayList<PlaceSearchResult>,
    private val listItemClickListener: ListItemClickListener
) : RecyclerView.Adapter<SearchListRVAdapter.SearchListRVViewHolder>() {

    inner class SearchListRVViewHolder(item: View, listItemClickListener: ListItemClickListener) :
        RecyclerView.ViewHolder(item) {
        val searchCard: MaterialCardView = item.findViewById(R.id.search_card)
        val searchName: TextView = item.findViewById(R.id.search_name)

        val clickListener: ListItemClickListener = listItemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchListRVViewHolder {
        val listItem = LayoutInflater.from(parent.context)
            .inflate(R.layout.search_list_item, parent, false) as LinearLayout

        return SearchListRVViewHolder(listItem, listItemClickListener)
    }

    override fun onBindViewHolder(holder: SearchListRVViewHolder, position: Int) {
        holder.searchName.text = data[position].fullName
        holder.searchCard.isChecked = data[position].isChecked

        holder.searchCard.setOnClickListener {
            holder.clickListener.searchHistoryItemClicked(position)
        }
    }

    override fun getItemCount(): Int = data.size

}