package com.example.trafficanalysisandroid.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.trafficanalysisandroid.Interfaces.ListItemClickListener
import com.example.trafficanalysisandroid.R
import com.google.android.material.card.MaterialCardView
import com.tomtom.online.sdk.search.data.fuzzy.FuzzySearchResult

class AutoCompleteRVAdapter(
    private val searchData: ArrayList<FuzzySearchResult>,
    private val listItemClickListener: ListItemClickListener
): RecyclerView.Adapter<AutoCompleteRVAdapter.AutoCompleteRVViewHolder>() {

    inner class AutoCompleteRVViewHolder(item: View, listItemClickListener: ListItemClickListener) :
        RecyclerView.ViewHolder(item) {
        val placeName: TextView = item.findViewById(R.id.place_name)
        // val placeDistance: TextView = item.findViewById(R.id.place_distance)
        val clickCard: MaterialCardView = item.findViewById(R.id.click_card)

        val clickListener: ListItemClickListener = listItemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AutoCompleteRVViewHolder {
        val listItem = LayoutInflater.from(parent.context)
            .inflate(R.layout.auto_complete_item, parent, false) as LinearLayout

        return AutoCompleteRVViewHolder(listItem, listItemClickListener)
    }

    override fun onBindViewHolder(holder: AutoCompleteRVViewHolder, position: Int) {
        val placeString = searchData[position].address.freeformAddress
        // val distanceString = "${searchData[position].distance}km"

        holder.placeName.text = placeString
        // holder.placeDistance.text = distanceString
        holder.clickCard.setOnClickListener {
            holder.clickListener.autoCompleteItemClicked(searchData[position])
        }
    }

    override fun getItemCount(): Int = searchData.size

}